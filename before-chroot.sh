#!/bin/bash
echo "take note that this script was made to be ran on a artixlinux install meduium"
pacman -Sy wget
mkdir /mnt/gentoo
cd /mnt/gentoo
echo "now to partision the disk"
echo "example layout:
root:/dev/sda2
boot:dev/sda1"
fdisk /dev/sda
echo "formating the drives"
mkfs.btrfs -f /dev/sda2
mkfs.fat -F32 /dev/sda1
echo "mounting the root partision"
mount /dev/sda2 /mnt/gentoo
echo "downloading the stage 3"
wget https://bouncer.gentoo.org/fetch/root/all/releases/amd64/autobuilds/20210317T214503Z/stage3-amd64-20210317T214503Z.tar.xz
echo "unpaking the stage 3"
tar xpvf stage3-*.tar.xz --xattrs-include='*.*' --numeric-owner
echo "mounting boot partision"
mount /dev/sda1 /mnt/gentoo/boot
echo "seting up make.conf"
rm /mnt/gentoo/etc/portage/make.conf
cp /root/ezgentoo/make.conf /mnt/gentoo/etc/portage/make.conf
echo "seting up package.use"
cp /root/ez/gentoo/linux-firmware /mnt/gentoo/etc/portage/package.use/linux-firmware
echo "seting up package.license file"
cp /root/gentoo/package.license /mnt/gentoo/etc/portage/package.license
echo "makeing fstab file"
rm /mnt/gentoo/etc/fstab
fstabgen -U /mnt/gentoo >> /mnt/gentoo/etc/fstab
echo "copying the dns info"
cp --dereference /etc/resolv.conf /mnt/gentoo/etc/
echo "copying after-chroot.sh to /mnt/gentoo"
cp /root/ezgentoo /mnt/gentoo
echo "chrooting into the system to continue the install run the after-chroot script in /mnt/gentoo"
artix-chroot /mnt/gentoo /bin/bash
source /etc/profile
export PS1="(chroot) ${PS1}"



