#!/bin/bash
echo "countinueing install from before-chroot.sh"
echo "Installing a Gentoo ebuild repository"
emerge-webrsync
echo "Updating the @world set"
emerge --ask --verbose --update --deep --newuse @world
echo "configureing the locale"
echo en_US ISO-8859-1 >> /etc/locale.gen
echo en_US.UTF-8 UTF-8 >> /etc/locale.gen
echo de_DE ISO-8859-1 >> /etc/locale.gen
echo de_DE.UTF-8 UTF-8 >> /etc/locale.gen
locale-gen
eselect locale set 8
echo "updateing the environment"
env-update && source /etc/profile && export PS1="(chroot) ${PS1}"
echo "installing utils"
emerge sys-kernel/genkernel sys-kernel/gentoo-sources sys-apps/pciutils sys-kernel/linux-firmware networkmanager
echo "copiling the kernel"
genkernel all
echo "configuring the network"
echo config_eth0="routes_eth0=config_eth0="dhcp" >> /etc/conf.d/net"
rc-update add NetworkManager default
emerge sys-apps/pcmciautils
echo "seting a password for root"
passwd root
echo "installing tools"
emerge sys-process/cronie
rc-update add cronie default
crontab /etc/crontab
emerge sys-apps/mlocate
rc-update add sshd default
emerge sys-fs/dosfstools sys-fs/btrfs-progs
emerge net-misc/dhcpcd emerge --ask net-dialup/ppp
echo "Configuring the bootloader"
emerge sys-boot/grub:2
emerge sys-boot/grub:2
grub-install --target=x86_64-efi --efi-directory=/boot
echo "install done you can now reboot"


